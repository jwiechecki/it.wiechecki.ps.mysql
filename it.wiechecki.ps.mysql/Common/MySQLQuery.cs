﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
#pragma warning disable 1591
namespace it.wiechecki.ps.mysql
{
    class MySQLQuery
    {
        public static DataSet SQLQuery(MySqlConnection Connection, string Query)
        {
            MySqlCommand mySqlCommand = new MySqlCommand();
            mySqlCommand.Connection = Connection;
            mySqlCommand.CommandText = Query;
            MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
            DataSet dataSet = new DataSet();
            mySqlDataAdapter.Fill(dataSet);
            return dataSet;
        }
    }
}
