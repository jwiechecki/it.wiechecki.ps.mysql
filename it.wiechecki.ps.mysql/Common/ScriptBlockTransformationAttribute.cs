﻿using System;
using System.Text.RegularExpressions;
using System.Management.Automation;

namespace it.wiechecki.ps.mysql
{
    /// <summary>
    /// Klasa przekształcająca blok skryptu Powershella zawierający aliasy $_ oraz $PSItem
    /// na blok zawierający $args[0]
    /// </summary>
    public class ScriptBlockTransformationAttribute : ArgumentTransformationAttribute
    {
        protected ScriptBlock RemoveAlias(ScriptBlock scriptBlock)
        {
            string str = scriptBlock.ToString().Replace("$_", "$args[0]");
            str = Regex.Replace(str, "[$][P|p][S|s][I|i][T|t][E|e][M|m]", "$args[0]");
            return ScriptBlock.Create(str);
        }
        protected Object RemoveAlias(Object inputData)
        {
            object input = inputData;
            if (input is PSObject)
            {
                input = ((PSObject)input).BaseObject;
            }
            if (input is ScriptBlock)
            {
                input = RemoveAlias(input as ScriptBlock);
            }
            return input;
        }
        protected Object[] RemoveAlias(Object[] inputData)
        {
            for (int i = 0; i < (inputData as Object[]).Length; ++i)
            {
                (inputData as Object[])[i] = RemoveAlias((inputData as Object[])[i]);
            }
            return inputData;
        }
        public override object Transform(EngineIntrinsics engineIntrinsics, object inputData)
        {
            return (inputData is Object[]) ? RemoveAlias(inputData as Object[]) : RemoveAlias(inputData);
        }
    }
}
