﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Globalization;
#pragma warning disable 1591
namespace it.wiechecki.ps.mysql
{
    public class DatabaseObjectNames
    {
        public enum ObjectNameKind { table, method};
        public static string[] GetNames(MySqlConnection connection, string query, ObjectNameKind kind)
        {
            return GetNames(connection, query, (int)kind);
        }
        private static string[] GetNames(MySqlConnection connection, string query, int itemIndex)
        {
            DataSet dataSet = MySQLQuery.SQLQuery(connection, query);
            string[] names = new string[dataSet.Tables[0].Rows.Count];
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; ++i)
            {
                names[i] = dataSet.Tables[0].Rows[i].ItemArray[itemIndex].ToString();
            }
            return names;
        }
    }

    public class DatabaseDump
    {
        public string DatabaseName { get; private set; } = string.Empty;

        public class CreateStatement
        {
            public string Name { get; set; } = string.Empty;
            public string Statement { get; set; } = string.Empty;
            public CreateStatement(MySqlConnection connection, string objectName, string query, int itemPosition)
            {
                this.Name = objectName;
                DataSet dataSet = MySQLQuery.SQLQuery(connection, query);
                Statement = Regex.Replace(dataSet.Tables[0].Rows[0].ItemArray[itemPosition].ToString(), @"\n", Environment.NewLine);
            }
        }

        public List<CreateStatement> TableStatements = new List<CreateStatement>();

        public class TableContent
        {
            public string TableName { get; private set; } = string.Empty;
            private StringBuilder InsertContent = new StringBuilder();
            public string TableData
            {
                get
                {
                    return InsertContent.ToString();
                }
            }

            public TableContent(MySqlConnection connection, string databaseName, string tableName)
            {
                this.TableName = tableName;
                DataSet dataSet = MySQLQuery.SQLQuery(connection, string.Format(QueryPatterns.SelectTableContent, databaseName, tableName));
                if (dataSet.Tables[0].Rows.Count == 0) return;
                this.InsertContent.Append(string.Format("INSERT INTO {0} VALUES ", tableName));
                int currentRow = 0;
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    StringBuilder fields = new StringBuilder();
                    for (int i = 0; i < dataSet.Tables[0].Columns.Count; i++)
                    {
                        Type currentType = dataSet.Tables[0].Columns[i].DataType;
                        string s = "NULL";
                        if (dataRow.ItemArray[i] != DBNull.Value)
                        {
                            if ((currentType == typeof(Boolean)) || (currentType == typeof(SByte)) || (currentType == typeof(Byte)) || (currentType == typeof(Int16)) || (currentType == typeof(Int32)) || (currentType == typeof(Int64))
                               || (currentType == typeof(UInt16)) || (currentType == typeof(UInt32)) || (currentType == typeof(UInt64)))
                            {
                                s = string.Format("{0}", dataRow.ItemArray[i].ToString());
                            }
                            else if (currentType == typeof(Decimal))
                            {
                                s = string.Format("{0}", ((decimal)dataRow.ItemArray[i]).ToString(CultureInfo.GetCultureInfo("en-US")));
                            }
                            else if (currentType == typeof(float))
                            {
                                s = string.Format("{0}", ((float)dataRow.ItemArray[i]).ToString(CultureInfo.GetCultureInfo("en-US")));
                            }
                            else if ((currentType == typeof(double)) || (currentType == typeof(Single)))
                            {
                                s = string.Format("{0}", ((double)dataRow.ItemArray[i]).ToString(CultureInfo.GetCultureInfo("en-US")));
                            }
                            else if (currentType == typeof(byte[]))
                            {
                                s = string.Format("0x{0}", BitConverter.ToString((byte[])dataRow.ItemArray[i]).Replace("-", string.Empty));
                            }
                            else
                            {
                                s = string.Format("'{0}'", dataRow.ItemArray[i].ToString());
                            }
                        }

                        if (i == 0)
                        {
                            fields.Append(string.Format("({0},", s));
                            continue;
                        }
                        else if (i == (dataSet.Tables[0].Columns.Count - 1))
                        {
                            fields.Append(string.Format("{0})", s));
                            continue;
                        }
                        else
                        {
                            fields.Append(string.Format("{0},", s));
                        }

                    }
                    if (currentRow == (dataSet.Tables[0].Rows.Count - 1))
                    {
                        InsertContent.AppendLine(string.Format("{0}", fields.ToString()));
                    }
                    else
                    {
                        InsertContent.AppendLine(string.Format("{0},", fields.ToString()));
                    }
                    currentRow++;
                }
            }
        }
        public List<TableContent> Content = new List<TableContent>();

        private void DumpTables()
        {
            string[] tableNames = DatabaseObjectNames.GetNames(currentConnection, string.Format(QueryPatterns.SelectTableNames, this.DatabaseName), DatabaseObjectNames.ObjectNameKind.table);
            DumpTables(tableNames);
        }

        private void DumpTables(string[] tableNames)
        {
            for (int i = 0; i < tableNames.Length; ++i)
            {
                try
                {
                    if (!SkipTable) this.TableStatements.Add(new CreateStatement(currentConnection, tableNames[i], string.Format(QueryPatterns.ShowCreateTable, this.DatabaseName, tableNames[i]), 1));
                    if (!SkipData) Content.Add(new TableContent(currentConnection, this.DatabaseName, tableNames[i]));
                }
                catch (Exception e)
                {
                    if (this.logException != null)
                    {
                        this.logException(new Exception(string.Format("Exception {0} during dump table: {1}", e.Message, tableNames[i])));
                    }
                }
            }
        }
        public class CreateViewStatement
        {
            public string Name { get; private set; } = string.Empty;
            public string Statement { get; private set; } = string.Empty;
            public string CharacterSetClient { get; private set; } = string.Empty;
            public string CharacterSetResults { get; private set; } = string.Empty;
            public string CollationConnection { get; private set; } = string.Empty;
            public CreateViewStatement(MySqlConnection connection, string objectName, string query)
            {
                this.Name = objectName;
                DataSet dataSet = MySQLQuery.SQLQuery(connection, query);
                Statement = Regex.Replace(dataSet.Tables[0].Rows[0].ItemArray[1].ToString(), @"\n", Environment.NewLine);
                CharacterSetClient = dataSet.Tables[0].Rows[0].ItemArray[2].ToString();
                CollationConnection = dataSet.Tables[0].Rows[0].ItemArray[3].ToString();
                SystemVariables systemVariables = new SystemVariables(connection);
                CharacterSetResults = systemVariables["character_set_results"];
            }
        }
        public List<CreateViewStatement> ViewStatements = new List<CreateViewStatement>();
        private void DumpViews()
        {
            string[] viewNames = DatabaseObjectNames.GetNames(currentConnection, string.Format(QueryPatterns.SelectViewNames, this.DatabaseName), DatabaseObjectNames.ObjectNameKind.table);
            for (int i = 0; i < viewNames.Length; i++)
            {
                try
                {
                    ViewStatements.Add(new CreateViewStatement(currentConnection, viewNames[i], string.Format(QueryPatterns.ShowCreateView, this.DatabaseName, viewNames[i])));
                }
                catch (Exception e)
                {
                    if (this.logException != null)
                    {
                        this.logException(new Exception(string.Format("Exception {0} during dump view: {1}", e.Message, viewNames[i])));
                    }
                }
            }
        }

        public List<CreateRoutineStatement> ProcedureStatements = new List<CreateRoutineStatement>();
        private void DumpProcedures()
        {
            string[] procedureNames = DatabaseObjectNames.GetNames(currentConnection, string.Format(QueryPatterns.SelectProcedureNames, this.DatabaseName), DatabaseObjectNames.ObjectNameKind.method);
            for (int i = 0; i < procedureNames.Length; ++i)
            {
                try
                {
                    ProcedureStatements.Add(new CreateRoutineStatement(currentConnection, procedureNames[i], string.Format(QueryPatterns.ShowCreateProcedure, this.DatabaseName, procedureNames[i])));
                }
                catch (Exception e)
                {
                    if (this.logException != null)
                    {
                        this.logException(new Exception(string.Format("Exception {0} during dump procedure: {1}", e.Message, procedureNames[i])));
                    }
                }
            }
        }

        public List<CreateRoutineStatement> FunctionStatements = new List<CreateRoutineStatement>();
        private void DumpFunctions()
        {
            string[] functionNames = DatabaseObjectNames.GetNames(currentConnection, string.Format(QueryPatterns.SelectFunctionNames, this.DatabaseName), DatabaseObjectNames.ObjectNameKind.method);
            for (int i = 0; i < functionNames.Length; ++i)
            {
                try
                {
                    FunctionStatements.Add(new CreateRoutineStatement(currentConnection, functionNames[i], string.Format(QueryPatterns.ShowCreateFunction, this.DatabaseName, functionNames[i])));
                }
                catch (Exception e)
                {
                    if (this.logException != null)
                    {
                        this.logException(new Exception(string.Format("Exception {0} during dump function: {1}", e.Message, functionNames[i])));
                    }
                }
            }
        }
        public class CreateEventStatement
        {
            public string Name { get; private set; } = string.Empty;
            public string SqlMode { get; private set; } = string.Empty;
            public string TimeZone { get; private set; } = string.Empty;
            public string Statement { get; private set; } = string.Empty;
            public string CharacterSetClient { get; private set; } = string.Empty;
            public string CollationConnection { get; private set; } = string.Empty;
            public string CollationDatabase { get; private set; } = string.Empty;
            public CreateEventStatement(MySqlConnection connection, string objectName, string query)
            {
                this.Name = objectName;
                DataSet dataSet = MySQLQuery.SQLQuery(connection, query);
                Statement = Regex.Replace(dataSet.Tables[0].Rows[0].ItemArray[3].ToString(), @"\n", Environment.NewLine);
                SqlMode = dataSet.Tables[0].Rows[0].ItemArray[1].ToString();
                TimeZone = dataSet.Tables[0].Rows[0].ItemArray[2].ToString();
                CharacterSetClient = dataSet.Tables[0].Rows[0].ItemArray[4].ToString();
                CollationConnection = dataSet.Tables[0].Rows[0].ItemArray[5].ToString();
                CollationDatabase = dataSet.Tables[0].Rows[0].ItemArray[6].ToString();
            }
        }

        public List<CreateEventStatement> EventStatements = new List<CreateEventStatement>();
        private void DumpEvents()
        {
            string[] eventNames = DatabaseObjectNames.GetNames(currentConnection, string.Format(QueryPatterns.SelectEventNames, this.DatabaseName), DatabaseObjectNames.ObjectNameKind.method);
            for (int i = 0; i < eventNames.Length; ++i)
            {
                try
                {
                    EventStatements.Add(new CreateEventStatement(currentConnection, eventNames[i], string.Format(QueryPatterns.ShowCreateEvent, this.DatabaseName, eventNames[i])));
                }
                catch (Exception e)
                {
                    if (this.logException != null)
                    {
                        this.logException(new Exception(string.Format("Exception {0} during dump event: {1}", e.Message, eventNames[i])));
                    }
                }
            }
        }

        public class CreateRoutineStatement
        {
            public string Name { get; private set; } = string.Empty;
            public string Statement { get; private set; } = string.Empty;
            public string SqlMode { get; private set; } = string.Empty;
            public string CharacterSetClient { get; private set; } = string.Empty;
            public string CharacterSetResults { get; private set; } = string.Empty;
            public string CollationConnection { get; private set; } = string.Empty;
            public string CollationDatabase { get; private set; } = string.Empty;
            public CreateRoutineStatement(MySqlConnection connection, string objectName, string query)
            {
                this.Name = objectName;
                DataSet dataSet = MySQLQuery.SQLQuery(connection, query);
                Statement = Regex.Replace(dataSet.Tables[0].Rows[0].ItemArray[2].ToString(), @"\n", Environment.NewLine);
                SqlMode = dataSet.Tables[0].Rows[0].ItemArray[1].ToString();
                CharacterSetClient = dataSet.Tables[0].Rows[0].ItemArray[3].ToString();
                CollationConnection = dataSet.Tables[0].Rows[0].ItemArray[4].ToString();
                CollationDatabase = dataSet.Tables[0].Rows[0].ItemArray[5].ToString();
                SystemVariables systemVariables = new SystemVariables(connection);
                CharacterSetResults = systemVariables["character_set_results"];
            }
        }

        public List<CreateRoutineStatement> TriggerStatements = new List<CreateRoutineStatement>();

        private void DumpTriggers()
        {
            string[] triggerNames = DatabaseObjectNames.GetNames(currentConnection, string.Format(QueryPatterns.SelectTriggerNames, this.DatabaseName), DatabaseObjectNames.ObjectNameKind.table);
            for (int i = 0; i < triggerNames.Length; ++i)
            {
                try
                {
                    TriggerStatements.Add(new CreateRoutineStatement(currentConnection, triggerNames[i], string.Format(QueryPatterns.ShowCreateTrigger, this.DatabaseName, triggerNames[i])));
                }
                catch (Exception e)
                {
                    if (this.logException != null)
                    {
                        this.logException(new Exception(string.Format("Exception {0} during dump trigger: {1}", e.Message, triggerNames[i])));
                    }
                }
            }
        }
        public Boolean SkipView { get; set; } = true;
        public Boolean SkipTrigger { get; set; } = true;
        public Boolean SkipProcedure { get; set; } = true;
        public Boolean SkipFunction { get; set; } = true;
        public Boolean SkipEvent { get; set; } = true;
        public Boolean SkipData { get; set; } = true;
        public Boolean SkipTable { get; set; } = true;
        public Boolean DropDatabase { get; set; } = true;
        public Boolean SkipDropTable { get; set; } = true;
        public Boolean SkipDropView { get; set; } = true;
        public Boolean SkipDropTrigger { get; set; } = true;
        public Boolean SkipDropProcedure { get; set; } = true;
        public Boolean SkipDropFunction { get; set; } = true;
        public Boolean SkipDropEvent { get; set; } = true;
        private void NewDump()
        {
            if (!SkipView) DumpViews();
            if (!SkipTrigger) DumpTriggers();
            if (!SkipProcedure) DumpProcedures();
            if (!SkipFunction) DumpFunctions();
            if (!SkipEvent) DumpEvents();
        }
        public void NewDatabaseDump()
        {            
            DumpTables();
            NewDump();
        }
        public void NewDatabaseDump(string[] tableNames)
        {            
            DumpTables(tableNames);
            NewDump();
        }

        private MySqlConnection currentConnection;
        private LogException logException;
        public DatabaseDump(MySqlConnection currentConnection, string databaseName, LogException logException)
        {
            this.currentConnection = currentConnection;
            this.DatabaseName = databaseName;
            this.logException = logException;
            MySQLQuery.SQLQuery(this.currentConnection, QueryPatterns.SetTimeZone);
            MySQLQuery.SQLQuery(this.currentConnection, string.Format(QueryPatterns.UseDatabase, this.DatabaseName));
        }
        public string Script
        {
            get
            {
                MySqlDatabaseDumpScript page = new MySqlDatabaseDumpScript(this);
                string script = page.TransformText();
                return script;
            }
        }
    }
}
