﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace it.wiechecki.ps.mysql
{
    #pragma warning disable 1591
    public class AvailableDatabases
    {
        public string[] Names { get; private set; }
        public AvailableDatabases(MySqlConnection connection)
        {
            //const string query = "SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE (SCHEMA_NAME<>'SYS') AND (SCHEMA_NAME<>'INFORMATION_SCHEMA') AND (SCHEMA_NAME<>'PERFORMANCE_SCHEMA')";
            DataSet dataSet = MySQLQuery.SQLQuery(connection, QueryPatterns.SelectAvaialableDatabaseNames);
            this.Names = new string[dataSet.Tables[0].Rows.Count];
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; ++i)
            {
                this.Names[i] = dataSet.Tables[0].Rows[i].ItemArray[1].ToString();
            }
        }
    }
}
