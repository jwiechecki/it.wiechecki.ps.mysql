﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
#pragma warning disable 1591
namespace it.wiechecki.ps.mysql
{
    public class UdfDump
    {
        private MySqlConnection currentConnection;
        public Boolean SkipDropUdf;
        public string Script
        {
            get
            {
                MySqlServerUdfDumpScript page = new MySqlServerUdfDumpScript(this);
                string script = page.TransformText();
                return script;
            }
        }
        public class UDFStatement
        {
            public string Name = string.Empty;
            public string Statement = string.Empty;
            public UDFStatement(string name, string statement)
            {
                this.Name = name;
                this.Statement = statement;
            }
        }
        public List<UDFStatement> CreateUdfStatements = new List<UDFStatement>();

        public void NewUdfDump()
        {
            try
            {
                DataSet dataSet = MySQLQuery.SQLQuery(this.currentConnection, "SELECT * FROM mysql.func");
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; ++i)
                {
                    switch (Convert.ToInt32(dataSet.Tables[0].Rows[i][1]))
                    {
                        case 0:
                            CreateUdfStatements.Add(new UDFStatement(dataSet.Tables[0].Rows[i][0].ToString(), string.Format("CREATE FUNCTION {0} RETURNS STRING SONAME `{1}`", dataSet.Tables[0].Rows[i][0].ToString(), dataSet.Tables[0].Rows[i][2].ToString())));
                            break;
                        case 1:
                            CreateUdfStatements.Add(new UDFStatement(dataSet.Tables[0].Rows[i][0].ToString(), string.Format("CREATE FUNCTION {0} RETURNS REAL SONAME `{1}`", dataSet.Tables[0].Rows[i][0].ToString(), dataSet.Tables[0].Rows[i][2].ToString())));
                            break;
                        case 2:
                            CreateUdfStatements.Add(new UDFStatement(dataSet.Tables[0].Rows[i][0].ToString(), string.Format("CREATE FUNCTION {0} RETURNS INTEGER SONAME `{1}`", dataSet.Tables[0].Rows[i][0].ToString(), dataSet.Tables[0].Rows[i][2].ToString())));
                            break;
                        case 3:
                            CreateUdfStatements.Add(new UDFStatement(dataSet.Tables[0].Rows[i][0].ToString(), string.Format("CREATE FUNCTION {0} RETURNS DECIMAL SONAME `{1}`", dataSet.Tables[0].Rows[i][0].ToString(), dataSet.Tables[0].Rows[i][2].ToString())));
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                if (this.logException != null)
                {
                    this.logException(e);
                }
            }
        }
        private LogException logException = null;
        public UdfDump(MySqlConnection currentConnection, LogException logException)
        {
            this.logException = logException;
            this.currentConnection = currentConnection;            
        }
    }
}
