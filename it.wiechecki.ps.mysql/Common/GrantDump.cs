﻿using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;

#pragma warning disable 1591
namespace it.wiechecki.ps.mysql
{
    public class GrantDump
    {
        public string Script
        {
            get
            {
                MySqlDatabaseGrantDumpScript page = new MySqlDatabaseGrantDumpScript(this);
                string script = page.TransformText();
                return script;
            }
        }

        public class UserHost
        {
            public string User { get; private set; }
            public string Host { get; private set; }
            public UserHost(string user, string host)
            {
                this.User = user;
                this.Host = host;
            }
        }
        private List<UserHost> Users(MySqlConnection connection, string query)
        {
            DataSet dataSet = MySQLQuery.SQLQuery(connection, query);
            List<UserHost> users = new List<UserHost>();
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; ++i)
            {
                users.Add(new UserHost(dataSet.Tables[0].Rows[i].ItemArray[0].ToString(), dataSet.Tables[0].Rows[i].ItemArray[1].ToString()));
            }
            return users;
        }

        private List<UserHost> Users(MySqlConnection connection)
        {
            return Users(connection, "SELECT USER, HOST FROM mysql.user");
        }

        private List<UserHost> Users(MySqlConnection connection, string[] userNames)
        {
            StringBuilder query = new StringBuilder();
            query.Append("SELECT HOST, USER FROM mysql.user WHERE ");
            StringBuilder users = new StringBuilder();
            for (int i = 0; i < userNames.Length; ++i)
            {
                users.Append((i != (userNames.Length - 1)) ? string.Format("(user={0}) or ", userNames[i]) : string.Format("(user={0})", userNames[i]));
            }
            query.Append(users);
            return Users(connection, query.ToString());
        }

        public List<string> CreateUserStatements = new List<string>();

        private void DumpUserStatements(MySqlConnection connection, List<UserHost> users)
        {
            string query = string.Empty;
            foreach (UserHost pair in users)
            {
                query = string.Format(QueryPatterns.ShowCreateUser, pair.User, pair.Host);                
                DataSet dataSet = MySQLQuery.SQLQuery(connection, query);
                CreateUserStatements.Add(dataSet.Tables[0].Rows[0].ItemArray[0].ToString());
            }
        }

        public List<string> CreateGrantStatements = new List<string>();

        private void DumpGrantStatements(MySqlConnection connection, List<UserHost> users)
        {
            string query = string.Empty;
            foreach (UserHost pair in users)
            {
                query = string.Format(QueryPatterns.ShowGrantsUser, pair.User, pair.Host);
                DataSet dataSet = MySQLQuery.SQLQuery(connection, query);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; ++i)
                {
                    CreateGrantStatements.Add(dataSet.Tables[0].Rows[i].ItemArray[0].ToString());
                }
            }
        }

        private void Dump(MySqlConnection connection, List<UserHost> users)
        {
            if (!this.SkipCreateUsers)
            {
                DumpUserStatements(connection, users);
            }
            DumpGrantStatements(connection, users);
        }
        public bool SkipCreateUsers { get; set; }
        public GrantDump(MySqlConnection connection, bool skipCreateUsers)
        {
            this.SkipCreateUsers = skipCreateUsers;
            List<UserHost> users = Users(connection);
            Dump(connection, users);
        }

        public GrantDump(MySqlConnection connection, string[] userNames, bool skipCreateUsers)
        {
            this.SkipCreateUsers = skipCreateUsers;
            List<UserHost> users = Users(connection, userNames);
            Dump(connection, users);
        }
    }
}

