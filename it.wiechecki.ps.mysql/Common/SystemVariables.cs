﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
#pragma warning disable 1591
namespace it.wiechecki.ps.mysql
{
    public delegate void LogException(Exception e);
    public class QueryPatterns
    {
        public const string SelectTriggerNames = "SELECT TRIGGER_NAME FROM INFORMATION_SCHEMA.TRIGGERS WHERE TRIGGER_SCHEMA='{0}'";
        public const string SelectTableNames = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='{0}' AND TABLE_TYPE ='BASE TABLE'";
        public const string SelectViewNames = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = '{0}'";
        public const string SelectTableContent = "SELECT * FROM {0}.{1}";
        public const string SelectProcedureNames = "SHOW PROCEDURE STATUS WHERE DB='{0}'";
        public const string SelectFunctionNames = "SHOW FUNCTION STATUS WHERE DB='{0}'";
        public const string SelectEventNames = "SHOW EVENTS FROM {0}";
        public const string SelectAvaialableDatabaseNames = "SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE (SCHEMA_NAME<>'SYS') AND (SCHEMA_NAME<>'INFORMATION_SCHEMA') AND (SCHEMA_NAME<>'PERFORMANCE_SCHEMA')";
        public const string ShowCreateUser = "SHOW CREATE USER `{0}`@`{1}`";
        public const string ShowCreateTable = "SHOW CREATE TABLE {0}.{1}";
        public const string ShowCreateView = "SHOW CREATE VIEW {0}.{1}";
        public const string ShowCreateProcedure = "SHOW CREATE PROCEDURE {0}.{1}";
        public const string ShowCreateFunction = "SHOW CREATE FUNCTION {0}.{1}";
        public const string ShowCreateEvent = "SHOW CREATE EVENT {0}.{1}";
        public const string ShowCreateTrigger = "SHOW CREATE TRIGGER {0}.{1}";
        public const string ShowGrantsUser = "SHOW GRANTS FOR `{0}`@`{1}`";
        public const string ShowVariables = "SHOW VARIABLES";
        public const string UseDatabase = "USE {0}";
        public const string SetTimeZone = "SET TIME_ZONE='+00:00'";
    }
    public class SystemVariables : Dictionary<string, string>
    {
        public string ServerVersion { get; private set; } = string.Empty;
        public SystemVariables(MySqlConnection connection)
        {
            this.ServerVersion = connection.ServerVersion;
            DataSet dataSet = MySQLQuery.SQLQuery(connection, QueryPatterns.ShowVariables);
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; ++i)
            {
                Add(dataSet.Tables[0].Rows[i].ItemArray[0].ToString(), dataSet.Tables[0].Rows[i].ItemArray[1].ToString());
            }
        }
    }
}
