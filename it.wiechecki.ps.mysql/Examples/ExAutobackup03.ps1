﻿# THIS IS WORKING DRAFT, NOT FOR PUBLICIZING
# (c) Jaroslaw Wiechecki
# http://wiechecki.it
# This script demonstrates how make dumps for local and remote databases using data from csv file.
# Format of csv file:
# UserName - contains name of user
# Password - contains encrypted password
# Host - address of MySqlServer
# DatabaseName - name of database
# DumpFile - name of dump file
#
# To prepare file with encrypted password, simply call:
# read-host -assecurestring | convertfrom-securestring | out-file "Path and name of file with password"
# When you create password in this way, you must to remember, that Password is encrypted with Windows Data Protection API. 
# This means, it is valid only for original machine and orginal user.
# If you want use this same script on different machines, you must create different files with password which will match 
# run context.

Clear-Host
Import-Module "Path\To\Module\it.wiechecki.ps.mysql.dll"
[DateTime] $date = [DateTime]::Now
[string] $sourceCsv = "Path\To\Csv\file.txt"
$databaseData = Import-Csv -Path $sourceCsv
$databaseData
foreach($item in $databaseData){
	[SecureString] $password = $item.password|ConvertTo-SecureString
	Connect-MySqlServer -Server $item.host -UserName $item.userName -Password $password
	New-MySqlDatabaseDump -Database $item.databaseName | Out-File $item.dumpFile
	Disconnect-MySqlServer
}

