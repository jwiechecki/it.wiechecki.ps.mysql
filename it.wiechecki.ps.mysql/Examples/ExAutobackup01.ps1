﻿# THIS IS WORKING DRAFT, NOT FOR PUBLICIZING
# (c) Jaroslaw Wiechecki
# http://wiechecki.it
# This script demonstrates how make dumps for local and remote databases.
# To do this Connect-MySqlServer is called with Credential object.
# To prepare file with encrypted password, simply call:
# read-host -assecurestring | convertfrom-securestring | out-file "Path and name of file with password"
# When you create password in this way, you must to remember, that Password is encrypted with Windows Data Protection API. 
# This means, it is valid only for original machine and orginal user.
# If you want use this same script on different machines, you must create different files with password which will match 
# run context.

Clear-Host
Import-Module "Path\To\Module\it.wiechecki.ps.mysql.dll"
[DateTime] $date = [DateTime]::Now

$passwordFile = "Path\To\Password\file1.txt"
$password = Get-Content $passwordFile|ConvertTo-SecureString
$userName = 'UserName1'
$serverAddress = 'server1'
$databaseName = 'database_name1'
$dumpFileName = 'Path\To\Dump\file_{0}{1}{2}{4}.sql' -f $date.Year, $date.Month, $date.Day, $date.Hour, $date.Minute
Connect-MySqlServer -Server $serverAddress -UserName $userName -Password $password
New-MySqlDatabaseDump -Database $databaseName | Out-File $dumpFileName
Disconnect-MySqlServer

$passwordFile = "Path\To\Password\file2.txt"
$password = Get-Content $passwordFile|ConvertTo-SecureString
$userName = 'UserName2'
$serverAddress = 'server2'
$databaseName = 'database_name2'
$dumpFileName = 'Path\To\Dump\file_{0}{1}{2}{4}.sql' -f $date.Year, $date.Month, $date.Day, $date.Hour, $date.Minute
Connect-MySqlServer -Server $serverAddress -UserName $userName -Password $password
New-MySqlDatabaseDump -Database $databaseName | Out-File $dumpFileName
Disconnect-MySqlServer
