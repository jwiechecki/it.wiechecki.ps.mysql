﻿# THIS IS WORKING DRAFT, NOT FOR PUBLICIZING
# (c) Jaroslaw Wiechecki
# http://wiechecki.it
# This script demonstrates how use Invoke-MySqlQuery cmdlet with objectes from pipeline.
# Script creates database with information about running processes. Every month has own database which contains
# table process gethering information about all processes and table for process which contains information about
# process.
Clear-Host
Import-Module "Path\To\Module\it.wiechecki.ps.mysql.dll"
[string] $PasswordFile = "Path\To\Password\file.txt"
[string] $userName = 'DatabaseUserName'
[SecureString] $password = Get-Content $PasswordFile|ConvertTo-SecureString
Connect-MySqlServer -Server server.address.com -UserName $userName -Password $password
[DateTime] $currentDate = [DateTime]::Now
[string] $databaseName = 'db_' + $currentDate.Year + '_' + $currentDate.Month;
$c=Invoke-MySqlQuery -Query "SELECT schema_name from information_schema.SCHEMATA where schema_name='$databaseName'"
if($c.schema_name -ne $databaseName)
{
    Write-Host "Create db: " + $databaseName   
    Invoke-MySqlQuery -Query "create database $databaseName"
    Invoke-MySqlQuery -Query "use $databaseName", "create table process(id int unsigned not null auto_increment, machine_name varchar(50), process_name varchar(50), process_path varchar(2048), pm_vm_sum int, insert_time timestamp not null default current_timestamp on update current_timestamp, primary key (id))", "commit"
}
# We block using backslashes as escape character. Therefore we can insert path with where only one backslash is used as path part separator.
# This lock will be valid only to end current connection and is valid only for this script.
Invoke-MySqlQuery -query "set sql_mode='no_backslash_escapes'"
# We invoke Get-Process cmdlet, which insert into pipeline information about running processes. 
# Invoke-MySqlQuery cmdlet executes three queries on every object obtained from pipeline.
# First query demonstrate how use variables and objects from pipeline:
# {"create table if not exists $databaseName.{0}(id int unsigned not null auto_increment, machine_name varchar(50), process_path varchar(2048), pm_vm_sum int, insert_time timestamp not null default current_timestamp on update current_timestamp, primary key (id))" -f ($_.ProcessName -replace '\.','_')}
# creates table with name matchin Processname. When you want to operate on objects from pipeline, use ScriptBlock when queries are assigned to Query parameter.
# Because processes can have dots in their names, we have to replace it by '_'. Next, new name of process is passed to query where replace {0}. $databaseName is replaced by Powershell.
# Second query insert information about process to table $databaseName.process:
# {"insert into $databaseName.process(machine_name,process_name,process_path,pm_vm_sum) values ('{0}', '{1}', '{2}', {3})" -f $_.MachineName, $_.ProcessName, $_.Path, ($_.PM + $_.VM)}
# Remember: always create pattern and next replace variables by object properties obtained from pipeline!
# Use: "select * from table_name where name='$_.PropertyName'" will not work! Correct is:
# "select * from table_name where name='{0}'" -f $_.PropertyName.
# Third query insert data into table which name match process name:
# {"insert into $databaseName.{0}(machine_name,process_path,pm_vm_sum) values ('{1}', '{2}', {3})" -f ($_.ProcessName -replace '\.','_'), $_.MachineName, $_.Path, ($_.PM + $_.VM)}
# Using Invoke-MySqlQuery you can parametrize all elements (table names, column names, data) not only data like in typical sql query.
Get-Process|Invoke-MySqlQuery -Query {"create table if not exists $databaseName.{0}(id int unsigned not null auto_increment, machine_name varchar(50), process_path varchar(2048), pm_vm_sum int, insert_time timestamp not null default current_timestamp on update current_timestamp, primary key (id))" -f ($_.ProcessName -replace '\.','_')}, {"insert into $databaseName.process(machine_name,process_name,process_path,pm_vm_sum) values ('{0}', '{1}', '{2}', {3})" -f $_.MachineName, $_.ProcessName, $_.Path, ($_.PM + $_.VM)}, {"insert into $databaseName.{0}(machine_name,process_path,pm_vm_sum) values ('{1}', '{2}', {3})" -f ($_.ProcessName -replace '\.','_'), $_.MachineName, $_.Path, ($_.PM + $_.VM)}
Disconnect-MySqlServer

