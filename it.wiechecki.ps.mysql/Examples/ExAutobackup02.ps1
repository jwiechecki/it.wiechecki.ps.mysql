﻿# THIS IS WORKING DRAFT, NOT FOR PUBLICIZING
# This script demonstrates how make dumps for local and remote databases.
# To do this Connect-MySqlServer is called with Credential object.
# To prepare file with encrypted password, simply call:
# read-host -assecurestring | convertfrom-securestring | out-file "Path and name of file with password"
# When you create password in this way, you must to remember, that Password is encrypted with Windows Data Protection API. 
# This means, it is valid only for orginal machine and orginal user.
# If you want use this same script on different machines, you must create different files with password which will mach 
# run context.

Clear-Host
Import-Module "d:\Users\LocalAdmin\Documents\Visual Studio 2015\Projects\it.wiechecki.ps.mysql\it.wiechecki.ps.mysql\bin\Debug\it.wiechecki.ps.mysql.dll"
[DateTime] $date = [DateTime]::Now

[string] $userName = 'UserName'
[string] $passwordFile = "Path\To\Password\file.txt"
[SecureString] $password = Get-Content $passwordFile|ConvertTo-SecureString
[System.Management.Automation.PSCredential] $credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userName, $password
[string] $serverAddress = 'server.address'
[string] $databaseName = 'database_name'
[string] $dumpFileName = 'Path\To\Dump\file_{0}{1}{2}{4}.sql' -f $date.Year, $date.Month, $date.Day, $date.Hour, $date.Minute

Connect-MySqlServer -Server $serverAddress -Credential $credential
New-MySqlDatabaseDump -Database $databaseName | Out-File $dumpFileName
Disconnect-MySqlServer

$userName = 'UserName'
$passwordFile = "Path\To\Server2\Password\file.txt"
$password = Get-Content $passwordFile|ConvertTo-SecureString
$credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userName, $password

$serverAddress = 'server2.address'
$databaseName = 'database_name'
$dumpFileName = 'Path\To\Dump\file_{0}{1}{2}{4}.sql' -f $date.Year, $date.Month, $date.Day, $date.Hour, $date.Minute
Connect-MySqlServer -Server $serverAddress -Credential $credential
New-MySqlDatabaseDump -Database $databaseName | Out-File $dumpFileName
Disconnect-MySqlServer

