﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#pragma warning disable 1591
namespace it.wiechecki.ps.mysql
{
    partial class MySqlServerDumpScriptHeader
    {
        private SystemVariables sysVariables;
        private string server;
        private string charset;
        public MySqlServerDumpScriptHeader(SystemVariables systemVariables, string charset, string server)
        {
            this.sysVariables = systemVariables;
            this.server = server;
            this.charset = charset;
        }
    }
}
