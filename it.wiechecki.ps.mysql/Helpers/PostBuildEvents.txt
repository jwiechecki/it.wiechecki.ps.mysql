﻿Orginal
md "$(ProjectDir)\Bin\Release"
xcopy "$(TargetPath)-Help.xml" "$(ProjectDir)\Bin\Release" /y
md "$(ProjectDir)\Bin\Debug"
xcopy "$(TargetPath)-Help.xml" "$(ProjectDir)\Bin\Debug" /y
md "$(ProjectDir)\Bin\Production"
xcopy "$(TargetPath)-Help.xml" "$(ProjectDir)\Bin\Production" /y
del "$(ProjectDir)\Bin\Documentation\*.dll"
del "$(ProjectDir)\Bin\Documentation\*.pdb"

Improved:
md "$(ProjectDir)\Bin\Release"
md "$(ProjectDir)\Bin\Debug"
md "$(ProjectDir)\Bin\Production\en-US"
md "$(ProjectDir)\Bin\Test\en-US"
md "$(ProjectDir)\Bin\Documentation"
del "$(ProjectDir)\Bin\Documentation\*.dll"
del "$(ProjectDir)\Bin\Documentation\*.pdb"
xcopy "$(ProjectDir)\Bin\Documentation\*-Help.xml" "$(ProjectDir)\Bin\Production\en-US" /y
xcopy "$(ProjectDir)\Bin\Documentation\*-Help.xml" "$(ProjectDir)\Bin\Debug\" /y
xcopy "$(ProjectDir)\Bin\Documentation\*-Help.xml" "$(ProjectDir)\Bin\Release\" /y
xcopy "$(ProjectDir)\Bin\Production\en-US\*-Help.xml" "$(ProjectDir)\Bin\Test\en-US\" /y
ren "$(ProjectDir)\Bin\Test\en-US\it.wiechecki.ps.mysql.dll-Help.xml" it.wiechecki.ps.mysql-test.dll-Help.xml
del "$(ProjectDir)\Bin\Test\*.*"
xcopy "$(ProjectDir)\Bin\Production\*.dll" "$(ProjectDir)\Bin\Test" /y
del "$(ProjectDir)\Bin\Test\it.wiechecki.ps.mysql-test.dll"
ren "$(ProjectDir)\Bin\Test\it.wiechecki.ps.mysql.dll" "it.wiechecki.ps.mysql-test.dll"


Path to script:
"D:\Users\LocalAdmin\moje dokumenty\visual studio 2015\projects\it.wiechecki.ps.mysql\it.wiechecki.ps.mysql\scripts"