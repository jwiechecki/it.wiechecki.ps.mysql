﻿using System;
using System.Management.Automation;
using System.Data;
using MySql.Data.MySqlClient;

namespace it.wiechecki.ps.mysql
{
    /// <summary>
    /// <para type="synopsis">Runs one or more sql queries.</para>
    /// <para type="description">Runs one or more sql queries which can process objects from pipeline.</para>
    /// <para type="link" uri="http://wiki.wiechecki.it/doku.php?id=mysqlcmdlet:invoke-mysqlquery">Read more</para>
    /// <example>
    /// <para>Common use</para>
    /// <code>Invoke-MySqlQuery -Query "Select * from DB001", "Select * from DB002"</code>
    /// </example>
    /// <example>
    /// <para>Data from pipeline</para>
    /// <code>Get-Process|Invoke-MySqlQuery -Query {"insert into $_.ProcessName(MachineName, Id, CPU) values($_.MachineName, $_.Id, $_.CPU)"}</code>
    /// </example>
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "MySqlQuery")]
    public class InvokeMySQLStatement: Cmdlet
    {
        /// <summary>
        /// <para type="description">
        /// Connection to MySql server. If not specified, connection opened by Connect-MySqlServer and stored in internal variable will be used.
        /// </para>
        /// </summary>
        [Parameter(Mandatory = false, Position = 0, ValueFromPipeline = true)]
        public MySqlConnection Connection { get; set; }
        /// <summary>
        /// <para type="description">
        /// Sql query to execute. This can be string or script.
        /// </para>
        /// </summary>
        [Parameter(Mandatory = true, Position = 1, ValueFromPipelineByPropertyName = true)]
        [ScriptBlockTransformation()]
        public Object[] Query { get; set; }
        /// <summary>
        /// <para type="description">
        /// Forces send objects from pipeline to output.
        /// </para>
        /// </summary>
        [Parameter()]
        public SwitchParameter PassThru { get; set; }
        /// <summary>
        /// <para type="description">
        /// Object from pipeline which will be processed.
        /// </para>
        /// </summary>
        [Parameter(ValueFromPipeline = true)]
        public Object InputObject { get; set; }

        private MySqlConnection internalConnection;
        #pragma warning disable 1591
        protected override void BeginProcessing()
        {
            base.BeginProcessing();
            if (this.Connection != null)
            {
                internalConnection = this.Connection;
                return;
            }
            else if(ModuleVariables.DefaultConnection != null)
            {
                internalConnection = ModuleVariables.DefaultConnection;
            }
            else
            {
                ArgumentException e = new ArgumentException("No connection to open");
                ErrorRecord errorRecord = new ErrorRecord(e, "MySqlDisconnectionException", ErrorCategory.ResourceUnavailable, ModuleVariables.DefaultConnection);
                ThrowTerminatingError(errorRecord);
            }
        }
        protected override void ProcessRecord()
        {            
            if(internalConnection.State == System.Data.ConnectionState.Closed)
            {
                WriteObject(internalConnection);
                return;
            }
            if(internalConnection != null)
            {

                string sqlQuery = string.Empty;
                foreach(Object obj in Query)
                {
                    if (obj.GetType() == typeof(ScriptBlock))
                    {
                        var result = (obj as ScriptBlock).Invoke(InputObject)[0];
                        sqlQuery = result.ToString();
                    }
                    else if (obj.GetType() == typeof(string))
                    {
                        sqlQuery = obj.ToString();
                    }
                    else
                    {
                        string msg = string.Format("Query parameter has bad type {0}", obj.GetType());
                        ArgumentException e = new ArgumentException(msg);
                        ErrorRecord errorRecord = new ErrorRecord(e, "MySqlBadQueryType", ErrorCategory.InvalidType, obj);
                        WriteError(errorRecord);                  
                    }
                    try
                    {
                        DataSet dataSet = MySQLQuery.SQLQuery(internalConnection, sqlQuery);
                        foreach (System.Data.DataTable tb in dataSet.Tables)
                        {
                            WriteObject(tb);
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorRecord errorRecord = new ErrorRecord(e, "MySql database error", ErrorCategory.InvalidOperation, sqlQuery);
                        WriteError(errorRecord);
                    }                    
                }
            }
            if (PassThru)
            {
                WriteObject(InputObject);
            }
        }
    }
}
