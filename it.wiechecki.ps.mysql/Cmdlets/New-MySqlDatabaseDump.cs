﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Management.Automation;
using System.Data;
using MySql.Data.MySqlClient;

namespace it.wiechecki.ps.mysql
{

    /// <para type="synopsis">Cmdlet makes dump of MySql databases.</para>
    /// <para type="description">
    /// This cmdlet makes dump one or more MySql databases. Cmdlet allows to choose which elements of database schema will be put 
    /// in dump file. Dump is returned as string.     
    /// </para>    
    /// <example>
    /// <code>
    /// New-MySqlDump -Database *
    /// </code> 
    /// <para>
    /// Dumps all databases from current server.
    /// </para>
    /// </example>
    /// <example>
    /// <code>
    /// New-MySqlDump -Datbase Sakila -Table Actor, Address
    /// </code>
    /// <para>
    /// Command makes dump Sakila database. Only tables Actor and Address will be included into dump. Rest of schema: views, triggers, procedures and other elements
    /// will be included too.
    /// </para>
    /// </example>
    /// <example>
    /// <code>
    /// New-MySqlDump -Database DB001, DB002 -SkipView -SkipProcedure -SkipFunction
    /// </code>
    /// <para>
    /// Cmdlet will dump DB001 and DB002 databases. Result data will contain all emements except views, procedures and functions.
    /// </para>
    /// </example>
    ///
    /// <para type="link" uri="http://wiki.wiechecki.it/doku.php?id=mysqlcmdlet:new-mysqldump">
    /// Default web page
    /// </para>
    /// 
    [Cmdlet(VerbsCommon.New, "MySqlDatabaseDump", DefaultParameterSetName = "Skip object")]
    [OutputType(typeof(string))]
    public class NewMySQLDump : Cmdlet, IDynamicParameters
    {
        #region Cmdlet parameters 
        /// <para type="description"> 
        /// Connection - object returned by Connect-MySqlServer cmdlet. If it is not present New-MySqlDump will use connection data stored in internal module variable.
        /// </para>
        [Parameter(Position = 0, ValueFromPipeline = true)]
        public MySqlConnection Connection { get; set; }
        /// <para type="description">
        /// Default character set for text data stored in dump file. Default value is utf8.
        /// </para>
        [Parameter()]
        public string DefaultCharacterSet { get; set; } = "utf8";
        /// <para type="description">
        /// Force cmdlet to store user grants.
        /// </para>
        [Parameter()]
        public SwitchParameter Grant { get; set; }
        /// <para type="description">
        /// Parameter contains names of users for which grants are dump. If this parameter in not set, grants for all users are stored in dump file.
        /// </para>
        [Parameter()]
        public string[] User { get; set; }
        /// <para type="description">
        /// Names of databases included in dump file. If this parameter equals to * schema for all databases (except system databases) are stored in dump file. 
        /// </para>
        [Parameter(Mandatory = true)]
        public string[] Database { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save create UDF function.
        /// </para>
        [Parameter()]
        public SwitchParameter IncludeUdf { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save DROP UDF IF EXISTS statement.
        /// </para>
        [Parameter()]
        public SwitchParameter SkipDropUdf { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to include script header statement.
        /// </para>
        [Parameter()]
        public SwitchParameter SkipScriptHeader { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to include script footer statement.
        /// </para>
        [Parameter()]
        public SwitchParameter SkipScriptFooter { get; set; }

#if Makedoc
#pragma warning disable 1591
        public class DynamicParameter { }
        /// <summary>
        /// <para type="description">
        /// Dynamic parameter. Real tyle string[]. Parameter is available when Database not equal to null,
        /// conatins only one database name and is not equal to *.
        /// </para>
        /// <para type="description">
        /// Contains names of tables which has to be included into dump.
        /// </para>
        /// </summary>
        [Parameter()]
        public DynamicParameter[] Table { get; set; }
#endif
        #region Switch for set "Skip objects"
        /// <para type="description">
        /// Force to skip create user statement in dump file.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipCreateUsers { get; set; }
        /// <para type="description">
        /// When set to true data from tables will not save in dump file.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipData { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save create table statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipTable { get; set; } 
        /// <para type="description">
        /// Parameter set to true forbids to save create view statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipView { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save create trigger statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipTrigger { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save create procedure statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipProcedure { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save create function statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipFunction { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save create event statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipEvent { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save DROP TABLE IF EXISTS statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipDropTable { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save DROP VIEW IF EXISTS statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipDropView { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save DROP TRIGGER IF EXISTS statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipDropTrigger { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save DROP PROCEDURE IF EXISTS statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipDropProcedure { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save DROP FUNCTION IF EXISTS statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipDropFunction { get; set; }
        /// <para type="description">
        /// Parameter set to true forbids to save DROP EVENT IF EXISTS statement.
        /// </para>
        [Parameter(ParameterSetName = "Skip object")]
        public SwitchParameter SkipDropEvent { get; set; }
        #endregion
        #region Switch set "Include only"
        /// <para type="description">
        /// When set to true data from tables will be include in dump file.
        /// </para>
        [Parameter(ParameterSetName = "Include only data")]
        public SwitchParameter OnlyData { get; set; }
        /// <para type="description">
        /// Parameter set to true include only create table statement.
        /// </para>
        [Parameter(ParameterSetName = "Include only tables")]
        public SwitchParameter OnlyTable { get; set; }
        /// <para type="description">
        /// Parameter set to true include only create view statement.
        /// </para>
        [Parameter(ParameterSetName = "Include only views")]
        public SwitchParameter OnlyView { get; set; }
        /// <para type="description">
        /// Parameter set to true include only create trigger statement.
        /// </para>
        [Parameter(ParameterSetName = "Include only triggers")]
        public SwitchParameter OnlyTrigger { get; set; }
        /// <para type="description">
        /// Parameter set to true include only create procedure statement.
        /// </para>
        [Parameter(ParameterSetName = "Include only procedures")]
        public SwitchParameter OnlyProcedure { get; set; }
        /// <para type="description">
        /// Parameter set to true include only create function statement.
        /// </para>
        [Parameter(ParameterSetName = "Include only function")]
        public SwitchParameter OnlyFunction { get; set; }
        /// <para type="description">
        /// Parameter set to true include only create event statement.
        /// </para>
        [Parameter(ParameterSetName = "Include only events")]
        public SwitchParameter OnlyEvent { get; set; }
        #endregion
        /// <para type="description">
        /// Parameter set to true force to save DROP DATABASE statement.
        /// </para>
        [Parameter()]
        public SwitchParameter DropDatabase;//dodaje drop database przed create
        #endregion
        #region Dynamic parameters
        #pragma warning disable 1591
        public class DynamicParameters
        {
            private string[] table;
            /// <para type="description">
            /// Names of tables included in dump file. This parameter is present only if Database parameter contains only one name of database. Otherwise all tables are put into dump file.
            /// </para>
            [Parameter()]
            public string[] Table
            {
                get
                {
                    return this.table;
                }
                set
                {
                    this.table = value;
                }
            }
        }
        private DynamicParameters dynamicParameters = null;        
        public Object GetDynamicParameters()
        {
            if ((Database != null) && (Database.Length == 1) && (!this.Database[0].Equals("*")))
            {
                this.dynamicParameters = new DynamicParameters();
                return this.dynamicParameters;
            }
            return null;
        }
        #pragma warning restore 1591
        #endregion
        #region Prywatne właściwości cmdletu. Connection, Script header and footer
        private void Log(Exception e)
        {
            ErrorRecord errorRecord = new ErrorRecord(e, "DumpException", ErrorCategory.WriteError, null);
            WriteError(errorRecord);
        }
        private string ScriptHeader
        {
            get
            {
                MySqlServerDumpScriptHeader page = new MySqlServerDumpScriptHeader(new SystemVariables(this.internalConnection), DefaultCharacterSet, "Server");
                string script = page.TransformText();
                return script;
            }
        }

        private string ScriptFooter
        {
            get
            {
                MySqlServerDumpScriptFooter page = new MySqlServerDumpScriptFooter();
                string script = page.TransformText();
                return script;
            }
        }

        #endregion
 
        private MySqlConnection internalConnection = null;
        #pragma warning disable 1591

        private void IncludeUdfBlock(StringBuilder script)
        {
            UdfDump udfDump = new UdfDump(this.internalConnection, Log);
            udfDump.SkipDropUdf = this.SkipDropUdf;
            udfDump.NewUdfDump();
            script.AppendLine(udfDump.Script);
        }
        private void IncludeGrantBlock(StringBuilder script)
        {
            GrantDump grantDump = (User == null) ? new GrantDump(this.internalConnection, this.SkipCreateUsers) : new GrantDump(this.internalConnection, this.User, this.SkipCreateUsers);
            script.AppendLine(grantDump.Script);
        }
        private void IncludeDatabaseBlock(StringBuilder script)
        {
            if ((Database.Length == 1) && (Database[0].Equals("*")))
            {
                AvailableDatabases availableDatabases = new AvailableDatabases(this.internalConnection);
                foreach (string dbName in availableDatabases.Names)
                {
                    NewDatabaseDump(dbName, script);
                }
            }
            else if ((Database.Length > 1))
            {
                foreach (string dbName in Database)
                {
                    NewDatabaseDump(dbName, script);
                }
            }
            else if ((Database.Length == 1) && (!Database[0].Equals("*")))
            {
                NewDatabaseDump(Database[0], script);
            }
        }

        protected override void BeginProcessing()
        {
            if (this.Connection != null)
            {
                internalConnection = this.Connection;
                return;
            }
            else if (ModuleVariables.DefaultConnection != null)
            {
                internalConnection = ModuleVariables.DefaultConnection;
            }
            else
            {
                ArgumentException e = new ArgumentException("No connection to open");
                ErrorRecord errorRecord = new ErrorRecord(e, "MySqlDisconnectionException", ErrorCategory.ResourceUnavailable, ModuleVariables.DefaultConnection);
                ThrowTerminatingError(errorRecord);
            }
        }
        #pragma warning restore 1591
 
        #pragma warning disable 1591
        protected override void ProcessRecord()
        {
            try
            {
                StringBuilder script = new StringBuilder();
                if (!SkipScriptHeader)
                {
                    script.AppendLine(ScriptHeader);
                }                
                if ((Grant != null) && (Grant))
                {
                    IncludeGrantBlock(script);
                }
                IncludeDatabaseBlock(script);
                if (IncludeUdf)
                {
                    IncludeUdfBlock(script);
                }
                if (!SkipScriptFooter)
                {
                    script.AppendLine(ScriptFooter);
                }                
                WriteObject(script.ToString());
            }
            catch (Exception e)
            {
                ErrorRecord errorRecord = new ErrorRecord(e, "ProcessError", ErrorCategory.InvalidOperation, null);
                ThrowTerminatingError(errorRecord);
            }
        }

        private void NewDatabaseDump(string databaseName, StringBuilder script)
        {
            DatabaseDump databaseDump = new DatabaseDump(this.internalConnection, databaseName, this.Log);
            if (this.OnlyData)
            {
                databaseDump.SkipData = false;
            }
            else if (this.OnlyEvent)
            {
                databaseDump.SkipEvent = false;
            }
            else if (this.OnlyFunction)
            {
                databaseDump.SkipFunction = false;
            }
            else if (this.OnlyProcedure)
            {
                databaseDump.SkipProcedure = false;
            }
            else if (this.OnlyTable)
            {
                databaseDump.SkipTable = false;
            }
            else if (this.OnlyTrigger)
            {
                databaseDump.SkipTrigger = false;
            }
            else if (this.OnlyView)
            {
                databaseDump.SkipView = false;
            }
            else
            {
                databaseDump.SkipView = this.SkipView;
                databaseDump.SkipTrigger = this.SkipTrigger;
                databaseDump.SkipProcedure = this.SkipProcedure;
                databaseDump.SkipFunction = this.SkipFunction;
                databaseDump.SkipEvent = this.SkipEvent;
                databaseDump.SkipData = this.SkipData;
                databaseDump.SkipTable = this.SkipTable;
                databaseDump.DropDatabase = this.DropDatabase;
                databaseDump.SkipDropTable = this.SkipDropTable;
                databaseDump.SkipDropView = this.SkipDropView;
                databaseDump.SkipDropTrigger = this.SkipDropTrigger;
                databaseDump.SkipDropProcedure = this.SkipDropProcedure;
                databaseDump.SkipDropFunction = this.SkipDropFunction;
                databaseDump.SkipDropEvent = this.SkipDropEvent;
            }
            if ((this.dynamicParameters != null) && (this.dynamicParameters.Table != null))
            {
                databaseDump.NewDatabaseDump(this.dynamicParameters.Table);
            }
            else
            {
                databaseDump.NewDatabaseDump();
            }
            script.AppendLine(databaseDump.Script);
        }
#pragma warning restore 1591
    }
}
