﻿using System;
using System.Management.Automation;
using MySql.Data.MySqlClient;

namespace it.wiechecki.ps.mysql
{
    /// <summary>
    /// <para type="synopsis">
    /// Cmdlet returns names of objects in database.
    /// </para>
    /// <para type="description">
    /// Cmdlet returns names of objects in database (tables, views etc).
    /// </para>
    /// <example>
    /// <para type="description">Returns names of all tables, views, triggers.</para>
    /// <code>Get-MySqlDatabaseItemName -Type Table,View,Trigger -DatabaseName sakila</code>
    /// </example>
    /// <para type="link" uri="http://wiki.wiechecki.it/doku.php?id=mysqlcmdlet:get-mysqldatabaseitemname"></para>
    /// </summary>
    [Cmdlet(VerbsCommon.Get, "MySqlDatabaseItemName")]
    public class GetMySqlDatabaseItemName : Cmdlet
    {
        /// <para type="description"> 
        /// Connection - object returned by Connect-MySqlServer cmdlet. If it is not present New-MySqlDump will use connection data stored in internal module variable.
        /// </para>
        [Parameter(Mandatory = false, Position = 0, ValueFromPipeline = true)]
        public MySqlConnection Connection { get; set; }
        public enum ItemType { Table, View, Trigger, Procedure, Function, Event, All }
        /// <summary>
        /// <para type="description">
        /// Type of object. Following types are available:
        /// </para>
        /// <para type="description">Table. Names of tables will be returned.</para>  
        /// <para type="description">View. Names of views will be returned.</para>
        /// <para type="description">Trigger. Names of triggers will be returned.</para>
        /// <para type="description">Procedure. Names of procedures will be returned.</para>
        /// <para type="description">Function. Names of function will be returned.</para>
        /// <para type="description">Event. Names of events will be returned.</para>
        /// <para type="description">All. Names of all objects will be returned. </para>
        /// </summary>
        [Parameter()]
        public ItemType[] Type { get; set; }
        /// <summary>
        /// <para type="description">Name of database from which names will be returned.</para>
        /// </summary>
        [Parameter(Mandatory = true)]
        public string DatabaseName { get; set; }
        /// <summary>
        /// <para type="description">Force to write name of object type.</para>
        /// </summary>
        [Parameter()]
        public SwitchParameter Source { get; set; }
#pragma warning disable 1591
        private void WriteNames(string[] names, ItemType type)
        {
            if (Source)
            {
                WriteObject(type.ToString());
            }
            WriteObject(names, true);
        }
        private void WriteNames(ItemType type)
        {
            switch (type)
            {
                case ItemType.Event:
                    //string[] tableNames = DatabaseObjectNames.GetNames(currentConnection, string.Format(QueryPatterns.SelectTableNames, this.DatabaseName), DatabaseObjectNames.ObjectNameKind.table);
                    WriteNames(DatabaseObjectNames.GetNames(internalConnection, string.Format(QueryPatterns.SelectEventNames, DatabaseName), DatabaseObjectNames.ObjectNameKind.method), ItemType.Event);
                    break;
                case ItemType.Function:
                    //WriteNames(DatabaseObjectNames.GetFunctionNames(internalConnection, DatabaseName), ItemType.Function);
                    WriteNames(DatabaseObjectNames.GetNames(internalConnection, string.Format(QueryPatterns.SelectFunctionNames, DatabaseName), DatabaseObjectNames.ObjectNameKind.method), ItemType.Function);
                    break;
                case ItemType.Procedure:
                    //WriteNames(DatabaseObjectNames.GetProcedureNames(internalConnection, DatabaseName), ItemType.Procedure);
                    WriteNames(DatabaseObjectNames.GetNames(internalConnection, string.Format(QueryPatterns.SelectProcedureNames, DatabaseName), DatabaseObjectNames.ObjectNameKind.method), ItemType.Procedure);
                    break;
                case ItemType.Table:
                    //WriteNames(DatabaseObjectNames.GetTableNames(internalConnection, DatabaseName), ItemType.Table);
                    WriteNames(DatabaseObjectNames.GetNames(internalConnection, string.Format(QueryPatterns.SelectTableNames, DatabaseName), DatabaseObjectNames.ObjectNameKind.table), ItemType.Table);
                    break;
                case ItemType.Trigger:
                    //WriteNames(DatabaseObjectNames.GetTriggerNames(internalConnection, DatabaseName), ItemType.Trigger);
                    WriteNames(DatabaseObjectNames.GetNames(internalConnection, string.Format(QueryPatterns.SelectTriggerNames, DatabaseName), DatabaseObjectNames.ObjectNameKind.table), ItemType.Trigger);
                    break;
                case ItemType.View:
                    //WriteNames(DatabaseObjectNames.GetViewNames(internalConnection, DatabaseName), ItemType.View);
                    WriteNames(DatabaseObjectNames.GetNames(internalConnection, string.Format(QueryPatterns.SelectViewNames, DatabaseName), DatabaseObjectNames.ObjectNameKind.table), ItemType.View);
                    break;
            }
        }
        protected override void ProcessRecord()
        {
            ItemType[] what = ((Type.Length == 1) && (Type[0] == ItemType.All)) ? new ItemType[] { ItemType.Table, ItemType.View, ItemType.Trigger, ItemType.Procedure, ItemType.Function, ItemType.Event, ItemType.All } : Type;
            for (int i = 0; i < what.Length; WriteNames(what[i]), ++i) ;
        }

        private MySqlConnection internalConnection = null;
        protected override void BeginProcessing()
        {
            base.BeginProcessing();
            if (this.Connection != null)
            {
                internalConnection = this.Connection;
                return;
            }
            else if (ModuleVariables.DefaultConnection != null)
            {
                internalConnection = ModuleVariables.DefaultConnection;
            }
            else
            {
                ArgumentException e = new ArgumentException("No connection to open");
                ErrorRecord errorRecord = new ErrorRecord(e, "MySqlDisconnectionException", ErrorCategory.ResourceUnavailable, ModuleVariables.DefaultConnection);
                ThrowTerminatingError(errorRecord);
            }
        }
#pragma warning restore 1591

    }
}
