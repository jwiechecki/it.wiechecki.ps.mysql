﻿using System;
using System.Text;
using System.Management.Automation;
using System.Security;

namespace it.wiechecki.ps.mysql
{
    /// <summary>
    /// <para type="synopsis">
    /// Connects to MySql server.
    /// </para>
    /// <para type="description">
    /// Connects to MySql server and establishes persistent connection to the MySql server. This connection is stored in internal variable of module and returned
    /// by the cmdlet as MySqlConnection which can be used by cmdlets: Invoke-MySqlQuery, Invoke-MySqlScript, New-MySqlDump and Disconnect-MySqlServer.
    /// </para>
    /// <para type = "link" uri="http://wiki.wiechecki.it/doku.php?id=mysqlcmdlet:connect-mysqlserver">
    /// Default web page
    /// </para>
    /// <example>
    /// <code>
    /// Connect-MySqlServer -Server Localhost -Credential (Get-Credential)
    /// </code>
    /// <para>
    /// Calls Get-Credential cmdlet and connect to MySql server on localhost computer.
    /// </para>
    /// </example>
    /// <example>
    /// <code>
    /// Connect-MySqlServer -ConnectionString "Server=myServerAddress;Port=1234;Database=myDataBase;Uid=myUsername;Pwd=myPassword;"
    /// </code>
    /// <para>
    /// Connects to MySql server using custom connection string with user name and password given as text.
    /// </para>
    /// </example>
    /// <example>
    /// <code>
    /// Connect-MySqlServer -ConnectionString "Server=myServerAddress;Port=1234;Database=myDataBase;" -Credential (Get-Credential)
    /// </code>
    /// <para>
    /// Asks for user credentials and connects to MySql server using custom connection string with user name and password taken from credential parameter.
    /// </para>
    /// </example>
    /// <example>
    /// <code>
    /// $pass=cat C:\securestring.txt | convertto-securestring
    /// Connect-MySqlServer -ConnectionString "Server=myServerAddress;Port=1234;Database=myDataBase" -UserName myUserName -Password $pass
    /// </code>
    /// <para>
    /// Connects to MySql server using custom connection string with user name and password read from file.
    /// </para>
    /// </example>
    /// <example>
    /// <code>
    /// Connect-MySqlServer -Server server1, server2, server3 -Credential (Get-Credential)
    /// </code>
    /// <para>
    /// Connects to MySql server in replicated server configuration.
    /// </para>
    /// </example>
    /// </summary>
    [Cmdlet(VerbsCommunications.Connect, "MySqlServer")]
    public class ConnectMySQL : Cmdlet, IDynamicParameters
    {
        /// <summary>
        /// <para type="description">
        /// Server address. Usually it is one address but when you want to connect to server in replicated server configuration write more then one address.
        /// </para>
        /// </summary>
        [Parameter(Mandatory = true, ParameterSetName = "Common connection parameters")]
        public string[] Server { get; set; }
        /// <summary>
        /// <para type="description">
        /// MySql server port. Default value is 3306.
        /// </para>
        /// </summary>
        [Parameter(Mandatory = false, ParameterSetName = "Common connection parameters")]
        public int Port
        {
            get;
            set;
        } = 3306;
        /// <summary>
        /// <para type="description">
        /// MySql datatabse which will be default for queries after connection.
        /// </para>
        /// </summary>
        [Parameter(Mandatory = false, ParameterSetName = "Common connection parameters")]
        public string Database
        {
            get;
            set;
        }
        /// <summary>
        /// <para type="description">
        /// Custom connection string. 
        /// </para>
        /// </summary>
        [Parameter(Mandatory = true, ParameterSetName = "Connection string")]
        public string ConnectionString
        {
            get;
            set;
        }
        /// <summary>
        /// <para type="description">
        /// Credential using to authenticate user.
        /// </para>
        /// </summary>
        [Parameter(Mandatory = false)]
        public PSCredential Credential
        {
            get;
            set;
        }
        #pragma warning disable 1591
        public class ManualUserNamePassword
        {
            private string userName;
            /// <summary>
            /// <para type="description">
            /// User name.
            /// </para>
            /// </summary>
            [Parameter()]
            public string UserName
            {
                get
                {
                    return this.userName;
                }
                set
                {
                    this.userName = value;
                }
            }
            private SecureString password;
            /// <summary>
            /// <para type="description">
            /// User password. This parameter works only with UserName. Use both parameters when cmdlet is called from script and password in secure form must be used.
            /// </para>
            /// </summary>
            [Parameter()]
            public SecureString Password
            {
                get
                {
                    return this.password;
                }
                set
                {
                    this.password = value;
                }
            }
        }
        #pragma warning restore 1591
        /// <summary>
        /// <para type="description">
        /// Default character set used for server with this connection.
        /// </para>
        /// </summary>
        [Parameter()]
        public string DefaultCharacterSet = "utf8";
        private ManualUserNamePassword manualUserNamePassword = null;
        #pragma warning disable 1591
        public object GetDynamicParameters()
        {
            if (this.Credential == null)
            {
                this.manualUserNamePassword = new ManualUserNamePassword();
                return this.manualUserNamePassword;
            }
            return null;
        }
        protected override void BeginProcessing()
        {
            try
            {
                string connectionString = string.Empty;
                if(this.ConnectionString != null)
                {
                    connectionString = this.ConnectionString;
                    if(this.Credential != null)
                    {
                        connectionString = string.Format("{0};uid={1};pwd={2};", connectionString, Credential.UserName, Credential.GetNetworkCredential().Password);
                    }
                    else if (this.manualUserNamePassword != null)
                    {
                        PSCredential psc = new PSCredential(this.manualUserNamePassword.UserName, this.manualUserNamePassword.Password);
                        connectionString = string.Format("{0};uid={1};pwd={2};", connectionString, Credential.UserName, psc.GetNetworkCredential().Password);
                    }
                }
                else if(Server != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("server=");
                    for (int i = 0; i < this.Server.Length; ++i)
                    {
                        if (i < this.Server.Length - 1)
                        {
                            sb.Append(string.Format("{0},", this.Server[i]));
                        }
                        else
                        {
                            sb.Append(string.Format("{0};", this.Server[i]));
                        }
                    }
                    sb.Append(string.Format("port={0};", this.Port));
                    if(this.Credential != null)
                    {
                        sb.Append(string.Format("uid={0};", this.Credential.UserName));
                        sb.Append(string.Format("pwd={0};", this.Credential.GetNetworkCredential().Password));
                    }
                    else if (this.manualUserNamePassword != null)
                    {
                        PSCredential psc = new PSCredential(this.manualUserNamePassword.UserName, this.manualUserNamePassword.Password);
                        sb.Append(string.Format("uid={0};", psc.UserName));
                        sb.Append(string.Format("pwd={0};", psc.GetNetworkCredential().Password));
                    }
                    if ((Database != null) && (Database != string.Empty))
                    {
                        sb.Append(string.Format("database={0};", this.Database));
                    }
                    sb.Append(string.Format("Convert Zero Datetime=True;"));
                    connectionString = sb.ToString();
                    sb.Clear();
                }
                ModuleVariables.DefaultConnection = new MySql.Data.MySqlClient.MySqlConnection(connectionString);
                ModuleVariables.DefaultConnection.Open();
                WriteObject(ModuleVariables.DefaultConnection);
            }
            catch (Exception e)
            {
                ErrorRecord errorRecord = new ErrorRecord(e, "MySqlConnectionException", ErrorCategory.ResourceUnavailable, ModuleVariables.DefaultConnection);
                ThrowTerminatingError(errorRecord);
            }
        }
        #pragma warning restore 1591
    }
}
