﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.Management.Automation;
using System.Data;
using System.IO;
using MySql.Data.MySqlClient;

namespace it.wiechecki.ps.mysql
{
    /// <summary>
    /// <para type="synopsis">Invokes one or more sql scripts.</para>
    /// <para type="description">Invokes MySql scripts.</para>
    /// <example>
    /// <para>Common use</para>
    /// <code>Invoke-MySqlScript -Script "d:\Scripts\script01.sql", "e:\Scripts\script02.sql"</code>
    /// </example>
    /// </summary>
    /// <para type="link" uri="http://wiki.wiechecki.it/doku.php?id=mysqlcmdlet:invoke-mysqlscript">
    /// Default web page
    /// </para>
    [Cmdlet(VerbsLifecycle.Invoke, "MySqlScript")]
    public class Invoke_MySqlScript : Cmdlet
    {
        /// <summary>
        /// <para type="description">
        /// Connection established with MySqlServer. If it is not given, cmdlet will use 
        /// connection stored in internal variable, which was earlier established by Connect-MySqlServer
        /// cmdlet.
        /// </para>
        /// </summary>
        [Parameter(Mandatory = false, Position = 0, ValueFromPipeline = true)]
        public MySqlConnection Connection;
        /// <summary>
        /// <para type="description">Paths to MySql scripts.</para>
        /// </summary>
        [Parameter(Mandatory = true, Position = 1)]
        public string[] Path;

        private MySqlConnection internalConnection;

        #pragma warning disable 1591
        protected override void BeginProcessing()
        {
            base.BeginProcessing();
            if (this.Connection != null)
            {
                internalConnection = this.Connection;
                return;
            }
            else if (ModuleVariables.DefaultConnection != null)
            {
                internalConnection = ModuleVariables.DefaultConnection;
            }
            else
            {
                ArgumentException e = new ArgumentException("No connection to open");
                ErrorRecord errorRecord = new ErrorRecord(e, "MySqlDisconnectionException", ErrorCategory.ResourceUnavailable, ModuleVariables.DefaultConnection);
                ThrowTerminatingError(errorRecord);
            }
        }
        protected override void ProcessRecord()
        {
            base.ProcessRecord();
            MySqlScript script = null;
            try
            {
                foreach(string s in Path)
                {
                    script = new MySqlScript(internalConnection, File.ReadAllText(s));
                    script.Execute();
                }
            }
            catch(Exception e)
            {
                ErrorRecord errorRecord = new ErrorRecord(e, "ProcessException", ErrorCategory.InvalidData, script);
                WriteError(errorRecord);
            }
        }
    }
}
