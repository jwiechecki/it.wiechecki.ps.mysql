﻿using System;
using System.Management.Automation;
using MySql.Data.MySqlClient;


namespace it.wiechecki.ps.mysql
{
    /// <summary>
    /// <para type="synopsis">
    /// Disconnects from MySql server.
    /// </para>
    /// <para type="description">
    /// Disconnects from MySql server.
    /// </para>
    /// <para type = "link" uri="http://wiki.wiechecki.it/doku.php?id=mysqlcmdlet:disconnect-mysqlserver">
    /// Default web page
    /// </para>
    /// <example>
    /// <para>
    /// Close default connection.
    /// </para>
    /// <code>
    /// Disconnect-MySqlServer
    /// </code>
    /// </example>
    /// <example>
    /// <code>
    /// Disconnect-MySqlServer -Connection $MyConnection
    /// </code>
    /// <para>
    /// Disconnect from server which address is stored in $MyConnection variable.
    /// </para>
    /// </example>
    /// </summary>
    [Cmdlet(VerbsCommunications.Disconnect, "MySqlServer")]
    public class DisconnectMySQLServer : Cmdlet
    {
        /// <summary>
        /// <para type="description">
        /// Connection to server which will be closed. If not given, connection stored in internal variable will be
        /// closed.
        /// </para>
        /// </summary>
        [Parameter(Mandatory = false, ValueFromPipeline = true, Position = 0)]
        public MySqlConnection Connection { get; set; }
        #pragma warning disable 1591
        protected override void BeginProcessing()
        {            
            try
            {
                if (this.Connection != null)
                {
                    this.Connection.Close();
                    if (this.Connection.Equals(ModuleVariables.DefaultConnection))
                    {
                        ModuleVariables.DefaultConnection.Close();
                        ModuleVariables.DefaultConnection = null;
                    }
                    WriteObject(this.Connection);                   
                }
                else if (ModuleVariables.DefaultConnection != null)
                {
                    ModuleVariables.DefaultConnection.Close();
                    WriteObject(ModuleVariables.DefaultConnection);
                    ModuleVariables.DefaultConnection = null;
                }
                else
                {
                    ArgumentException e = new ArgumentException("No connection to close");
                    ErrorRecord errorRecord = new ErrorRecord(e, "MySqlDisconnectionException", ErrorCategory.ResourceUnavailable, ModuleVariables.DefaultConnection);
                    WriteError(errorRecord);
                }
            }
            catch (Exception e)
            {
                ErrorRecord errorRecord = new ErrorRecord(e, "MySqlDisconnectionException", ErrorCategory.ResourceUnavailable, ModuleVariables.DefaultConnection);
                ThrowTerminatingError(errorRecord);
            }
        }
    }
}
